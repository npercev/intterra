import Operation from '../models/Operation';
import TDate from '../models/TDate';

export default class Sorts {
  static dateSort(a: Operation, b: Operation): number {
    return TDate.toDate(a.date) > TDate.toDate(b.date) ? 1 : -1;
  }

  static typeSort(a: Operation, b: Operation): number {
    return a.type > b.type ? 1 : -1;
  }

  static assessmentSort(a: Operation, b: Operation): number {
    if (a.assessment === null) {
      return -1;
    }
    if (b.assessment === null) {
      return 1;
    }
    return Number(a.assessment) > Number(b.assessment) ? 1 : -1;
  }
}
