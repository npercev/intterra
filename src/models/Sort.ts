export enum SortDirection {
  DECREASE = -1,
  NONE = 0,
  INCREASE = 1,
}

export enum SortType {
  DATE = 1,
  TYPE = 2,
  ASSESSMENT = 3,
}
