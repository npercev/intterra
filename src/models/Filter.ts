enum OperationFilter {
  PLANNED = -1,
  NONE = 0,
  COMPLETED = 1,
}
export default OperationFilter;
