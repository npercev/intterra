import Operation from './models/Operation';

export interface State {
  operations: Array<Operation>;
  errors: boolean;
  loading: boolean;
}
