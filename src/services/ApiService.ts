import FieldService from './FieldService';
import Operation, { OperationType, Assessment } from '../models/Operation';

class ApiService {
  private fieldService: FieldService;

  private operations: Array<Operation>;

  constructor() {
    this.fieldService = new FieldService();
    this.operations = [];
  }

  async getOperations() {
    const response = await this.fieldService.getOperations();
    return response;
  }

  async getOperation(id: string | null) {
    if (!id) {
      return null;
    }
    return (
      this.operations.find((el) => el.id === id) ||
      this.fieldService.getOperation(id)
    );
  }

  async getFirstOperationId() {
    return this.operations[0].id;
  }

  async setNewOperation() {
    const newOperation = new Operation({
      id: null,
      type: OperationType.HARVESTING,
      date: { year: 2018, month: 9, day: 25 },
      area: 85.5,
      comment: 'New Test Comment',
      assessment: Assessment.SATISFACTORILY,
    });

    const response = await this.fieldService.saveOperation(newOperation);
    return response;
  }
}

const apiService = new ApiService();

export default apiService;
