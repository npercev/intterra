import Vue from 'vue';
import Vuex from 'vuex';

import apiService from '../services/ApiService';

import { State } from '../interfaces';
import Operation from '../models/Operation';
import { SortType, SortDirection } from '../models/Sort';
import OperationFilter from '../models/Filter';

import Sorts from '../utils/sorts';

Vue.use(Vuex);

const state: State = {
  operations: [],
  errors: false,
  loading: false,
};

export default new Vuex.Store({
  state,
  mutations: {
    setOperations: (state, operations: Operation[]) =>
      (state.operations = operations),
    loading: (state, loading: boolean) => (state.loading = loading),
  },
  getters: {
    getOperations: (state) => (
      sortType: SortType,
      sortDirection: SortDirection,
      filter: OperationFilter
    ) => {
      let { operations } = state;
      if (filter === OperationFilter.COMPLETED) {
        operations = operations.filter(
          (operation) => operation.assessment !== null
        );
      } else if (filter === OperationFilter.PLANNED) {
        operations = operations.filter(
          (operation) => operation.assessment === null
        );
      }
      switch (sortType) {
        case SortType.DATE:
          return sortDirection === SortDirection.INCREASE
            ? operations.sort(Sorts.dateSort)
            : operations.sort(Sorts.dateSort).reverse();
        case SortType.TYPE:
          return sortDirection === SortDirection.INCREASE
            ? operations.sort(Sorts.typeSort)
            : operations.sort(Sorts.typeSort).reverse();
        case SortType.ASSESSMENT:
          return sortDirection === SortDirection.INCREASE
            ? operations.sort(Sorts.assessmentSort)
            : operations.sort(Sorts.assessmentSort).reverse();
        default:
          return state.operations.sort(Sorts.dateSort);
      }
    },
  },
  actions: {
    async fetchOperations({ commit }: any) {
      try {
        const response = await apiService.getOperations();
        commit('setOperations', response);
      } catch (e) {
        console.log('error', e);
      } finally {
        commit('loading', false);
      }
    },
  },
  modules: {},
});
