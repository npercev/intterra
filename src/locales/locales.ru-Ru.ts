const locales = {
  PLOWING: 'Вспашка',
  BOWLING: 'Боронование',
  FERTILIZATION: 'Внесение удобрений',
  WATERING: 'Полив',
  RIGGING: 'Прикатывание',
  HARVESTING: 'Сбор урожая',
  EXCELLENT: 'Отлично',
  SATISFACTORILY: 'Удовлетворительно',
  BADLY: 'Плохо',
} as {
  [key: string]: string;
};

export default locales;
